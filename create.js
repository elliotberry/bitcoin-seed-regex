const bip39 = require('bip39js');
 
const size = 128; // 12 words
const entropy = bip39.genEntropy(size);
const mnemonic = bip39.genMnemonic(entropy);
console.log(mnemonic);